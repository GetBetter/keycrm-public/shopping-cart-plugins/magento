<?php

namespace Stagem\KeyCrm\Model\Api;

class Buyer
{
    /** @var string */
    public $full_name;
    /** @var string */
    public $email;
    /** @var string */
    public $phone;
}

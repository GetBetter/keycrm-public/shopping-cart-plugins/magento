# Magento

## How to install

Step 1: Upload the directory to app/code/ into the root directory of your Magento installation. All directories should match the existing directory structure

Step 2: Go to Magento 2 root directory. Run: php bin/magento setup:upgrade.

Step 3: Run: php bin/magento setup:static-content:deploy.

Step 4: Clear all Cache.


